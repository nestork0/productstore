# ProductStore

## Prerequisites

Install JDK 8 by [following these instructions](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html#CJAGAACB)

Install play framework 1.4.3 [by following these instructions](https://www.playframework.com/documentation/1.4.x/install)

Install MySQL 5.6 [by following these instructions](https://dev.mysql.com/doc/refman/5.6/en/installing.html)

Register for Stripe Api Keys [here](https://stripe.com/docs/development#api-keys)

## Downloading Application

In order to download application you need to clone it.

```git clone git@bitbucket.org:nestork0/productstore.git ProductStore```


## Running application

Application needs Stripe test credentials and MySQl Db credentials in order to start.
Http port is set as 9000.

Open file `conf/application.conf` and edit first 3 lines to add required params:

* ```application.stripeApiKey=KEY - Stripe key```

* ```db=mysql:user:password@db - login, password and db name for the MySQL connection. Assumed connection to the localhost. You will need to add host if connecting to anything other than localhost. Make sure user has permissions to create tables.```

* ```http.port=9000 - application port```


You will need play framework version 1.4.x installed to run application.

Start command

```play run ProductStore```


Open web browser and go to http://localhost:9000

Sample data will be automatically populated in database
