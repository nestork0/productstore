import models.Product;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;
 
@OnApplicationStart
public class Bootstrap extends Job {
 
	public void doJob() {

		if(Product.count() == 0) {
			System.out.println("Loading");
			Fixtures.loadModels("initial-data.yml");
		}
	}
 
}
