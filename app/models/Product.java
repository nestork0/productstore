package models;

import javax.persistence.Entity;

import play.db.jpa.Model;

@Entity
public class Product extends Model {
    public String name;
    public String description;
    public String imagePath;
    public Double price;
    
	public Product(String name, String description, String imagePath, Double price) {
		super();
		this.name = name;
		this.description = description;
		this.imagePath = imagePath;
		this.price = price;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append(", imagePath=");
		builder.append(imagePath);
		builder.append(", price=");
		builder.append(price);
		builder.append(", getId()=");
		builder.append(getId());
		builder.append("]");
		return builder.toString();
	}
    
}
