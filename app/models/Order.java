package models;

import javax.persistence.Entity;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.Model;

@Entity(name="Orders")
public class Order extends Model {
	
	@Required(message = "First name required")
	public String firstName;
	
	@Required(message = "Last name required")
	public String lastName;
	
	@Required(message = "Address required")
	public String address;
	
	@Required(message = "City required")
	public String city;
	
	@Required(message = "State required")
	public String state;
	
	@Required(message = "Zip required")
	@MaxSize(value=5, message="Zip needs to by 5 digits")
	public Integer zip;

	public String paymentReference;
	
	@Required(message = "Amount required")
	public Double amount;
	
	public Order(String firstName, String lastName, String address, String city, String state, Integer zip,
			String paymentReference, Double amount) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.paymentReference = paymentReference;
		this.amount = amount;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Order [firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", address=");
		builder.append(address);
		builder.append(", city=");
		builder.append(city);
		builder.append(", state=");
		builder.append(state);
		builder.append(", zip=");
		builder.append(zip);
		builder.append(", paymentReference=");
		builder.append(paymentReference);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", getId()=");
		builder.append(getId());
		builder.append("]");
		return builder.toString();
	}
	
	
}
