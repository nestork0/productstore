package controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.stripe.Stripe;
import com.stripe.model.Charge;

import models.Order;
import models.Product;
import play.Play;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.data.validation.Validation;
import play.mvc.Controller;

public class Application extends Controller {

	private static final Integer STRIPE_AMOUNT_OFFSET = 100;
	
    static {
        Stripe.apiKey = Play.configuration.getProperty("application.stripeApiKey", "");
        //sk_test_FrFw2wplUiUi9I0xKWnh7HMp
    }

	
    public static void index() {
    	List<Product> products = Product.findAll();
        render(products);
    }
    
    public static void order(Long id) {
		Product p = Product.findById(id);
		render(p);
    }
    
    public static void placeOrder(
    		@Valid Order order, 
    		@Valid @Required(message="Card number required") String cardNumber, 
    		@Valid @Required(message="Expiration required") String expiration, 
    		@Valid @Required(message="CVV required") String cvv,
    		Long productId) {
    	
    	checkAuthenticity();
    	validateOrderRequest(order, cardNumber, expiration);
        
    	if(validation.hasErrors()) {
            params.flash();
            validation.keep();
            order(productId);
        } else {
        	try {
        		order.save();
				stripe(order, cardNumber, expiration, cvv);
			} catch (Exception e) {
				flash.put("error", e.getMessage());
			}
        	render(order);
        }
    }
    
    private static void validateOrderRequest(Order order, String cardNumber, String expiration) {
        if (!order.firstName.matches("[a-zA-Z']+") ) {
            Validation.addError("order.firstName", "Contains not allowed characters");
        }
        if (!order.lastName.matches("[a-zA-Z']+") ) {
            Validation.addError("order.lastName", "Contains not allowed characters");
        }
        
        if (!cardNumber.matches("[0-9]+") ) {
        	Validation.addError("cardNumber", "Card number can contain only digits");
        }
        
        if (cardNumber.length() < 15 || cardNumber.length() < 16 ) {
        	Validation.addError("cardNumber", "Card number length is incorrect");
        }
    }
    
    private static String stripe(Order order, String cardNumber, String expiration, String cvv) throws Exception {
    	Map<String, Object> chargeParams = new HashMap<>();
    	chargeParams.put("amount", (int) (order.amount*STRIPE_AMOUNT_OFFSET));
    	chargeParams.put("currency", "usd");
    	chargeParams.put("source", "tok_visa");

    	Map<String, Object> initialMetadata = new HashMap<>();

    	initialMetadata.put("order_id", order.getId());
    	chargeParams.put("metadata", initialMetadata);

    	Charge charge = Charge.create(chargeParams);
    	if ( charge.getStatus().equals("succeeded")) {
    		order.paymentReference = charge.getId();
    		order.save();
    		return charge.getId();
    	} else {
    		throw new Exception(charge.getFailureMessage());
    	}
    }

}